/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

import $ from 'jquery';

$( "#shah" ).mouseover(function() {
    var slider = $( '.slick-slider' );
    slider[0].slick.slickGoTo(1);
});

$( "#zarka" ).mouseover(function() {
    var slider = $( '.slick-slider' );
    slider[0].slick.slickGoTo(2);
});

$( "#hajjar" ).mouseover(function() {
    var slider = $( '.slick-slider' );
    slider[0].slick.slickGoTo(3);
});

$( "#sbaity" ).mouseover(function() {
    var slider = $( '.slick-slider' );
    slider[0].slick.slickGoTo(4);
});

$( "#esmaeili" ).mouseover(function() {
    var slider = $( '.slick-slider' );
    slider[0].slick.slickGoTo(5);
});

$( "#arsenault" ).mouseover(function() {
    var slider = $( '.slick-slider' );
    slider[0].slick.slickGoTo(6);
});


$(function () {
    $('.animated').addClass('activated');

    $('.animate-page-change').click(function (e) {
        e.preventDefault();

        $('.activated').removeClass('activated');

        setTimeout(() => {
            window.location.href = $(this).attr('href');
        }, 1000)
    });

    $('.more-info-links').click(function (e) {
        e.preventDefault();

        const $hide = $(this).data('hide');
        const $show = $(this).data('show');

        $("#" + $hide).hide();
        $("#" + $show).show(200);
    });

    let isSubmitting = false;
    $('#contact-form').submit(function (e) {
        e.preventDefault();

        if (!isSubmitting) {
            isSubmitting = true;

            axios.post('/contact', objectifyForm($(this))).then(() => {
                isSubmitting = false;
                $('#contact-modal').modal('hide');
                $(this).find('.form-group').removeClass('has-error').find('.help-block').remove();
                // $('#contact-submission-success-modal').modal('show');
                // window.location.replace("http://coastal.test/thank-you");
                window.location.href = "/thank-you"
            }).catch((error) => {
                isSubmitting = false;
                Object.keys(error.response.data).forEach((field) => {
                    $(`[name="${field}"]`).closest('.form-group').addClass('has-error').append(`<span class="help-block">${error.response.data[field]}</span>`);
                })
            });
        }
    });

    $('#contact-form input, #contact-form textarea').focus(function () {
        $(this).closest('.form-group').removeClass('has-error').find('.help-block').remove();
    });
});

$('#menu-open-button').click(function (e) {
    e.preventDefault();
    $('#menu').slideDown();
});

$('#menu-close-button').click(function (e) {
    e.preventDefault();
    $('#menu').slideUp();
});

$('.children-links').click(function () {
    $('#menu').slideUp();
});

// serialize data function
function objectifyForm($form) {
    const formArray = $form.serializeArray();
    const returnArray = {};

    for (let i = 0; i < formArray.length; i++){
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }

    return returnArray;
}

$(document).ready(function () {
    $('.main-image-container').slick({
        arrows: false,
        slidesToShow: 1,
        slidesToScroll:1,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 500,
        cssEase: 'ease-in-out'
    });

    $('.spider-treatment-container').slick({
        arrows: false,
        slidesToShow: 1,
        slidesToScroll:1,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 500,
        cssEase: 'ease-in-out'
    });
});