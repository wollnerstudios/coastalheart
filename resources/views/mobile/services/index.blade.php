@extends('mobile')

@section('content')
	<div class="page service-page">
		<div class="container">
			<div class="row">
		    	<img class="banner-image" src="/images/mobile/services-angiography-mobile.jpg">
		    </div>
		    <div class="row bg-theme-brown home-menu text-center physicians-title " id="angiography-facilities">
		    	SERVICES: Angiography Facilities		
		    </div>
		    <p class="informative-text">
		    	We want to provide our partients with more solutions- the kind they can't get anywhere else. That's why Coastal Heart Medical Group is the only one in Orange County, CA with its own in-house Angiography Facilities. The facility can perform and diagnose arterial and venous narrowing or obstuction. We offer a variety of procedures including angioplasty w/balloon, atherectomy to peel off the plaque, and stent to the arteries and veins.
		    </p>
		    <h4>Our Services Include:</h4>
		    <ul>
		    	<li>Identify narrowed, enlarged, and blocked blood vessels</li>
		    	<li>Identify blocked and hardened</li>
		    	<li>Test for blood clots </li>
		    	<li>Venous ablation </li>
		    </ul>
		    <div class="row">
		    	<img class="banner-image" src="/images/mobile/services-studies-mobile.jpg">
		    </div>
		    <div id="venous-studies-treatments" class="row bg-theme-brown home-menu text-center physicians-title ">
		    	SERVICES: Venous Studies & Treatments		
		    </div>
            <p class="informative-text">
            	Venous studies are performed at Coastal Heart’s advanced vascular diagnostic and treatment center, to detect the location and severity of venous and arterial vascular diseases. These are state-of-the-art, cost-effective and     painless procedures, performed by our cardiologists who are credentialed in vascular angiography medicine and vascular intervention. We are proud to announce that just opened our own interventional suite for varicose diseases.
            </p>
		    <h4>Treatment and procedures:</h4>
		    <ul>
                @foreach($treatments as $treatment)
                    <li>
                    	{{ $treatment['name'] }} <br>
                    	<div id="treatment-{{ str_slug($treatment['name']) }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            {!! $treatment['description'] !!}
                        </div>
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="more-info-links"
                           href="#treatment-{{ str_slug($treatment['name']) }}" aria-expanded="true" aria-controls="treatment-{{ str_slug($treatment['name']) }}">
                            Click for more info >
                        </a>
                        
                    </li>
                @endforeach
            </ul>
            <br>
		    <div class="row">
		    	<img class="banner-image" src="/images/mobile/services-diagnostics-mobile.jpg">
		    </div>
		    <div class="row bg-theme-brown home-menu text-center physicians-title " id="diagnostic-testing">
		    	SERVICES: Diagnostic Testing		
		    </div>
		 
	   		<p class="informative-text">
				Our facilities in Orange County include a wide range of noninvasive cardiovasular capabilities. Our experienced cardiologists use the latest technology to detect heart disease and discover potential problems. Our available testing services include:
			</p>

			<ul>
			    <li>Treadmill stress</li>
			    <li>Echocardiogram and Doppler plus color flow imaging</li>
			    <li>Nuclear stress test</li>
			    <li>Arterial ultrasound and ABI</li>
			    <li>Holder monitoring</li>
			    <li>Arrythmia management</li>
			</ul>

		</div>
	</div>
@endsection