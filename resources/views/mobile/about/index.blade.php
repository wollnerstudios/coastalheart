@extends('mobile')

@section('content')
	<div class="page">
		<div class="container">
			<div class="row">
		    	<img class="banner-image" src="/images/mobile/about-group-mobile.jpg">
		    </div>
		    <div class="row bg-theme-brown home-menu text-center physicians-title " id="staff">
		    	ABOUT US: Staff		
		    </div>
		    <p class="informative-text">
		    	Our winning team is the coastal heart medical group leader in cardiovascular health. For more than 30 years we’ve provided the highest level of care utilizing the latest technology.  All of our physicians are Board Certified in Cardiovascular Diseases and Internal Medicine. They are trained and qualified in every aspect of non-invasive, invasive and interventional cardiology.
		    </p>
		    <div class="row">
		    	<img class="banner-image" src="/images/mobile/about-location-mobile.jpg">
		    </div>
		    <div class="row bg-theme-brown home-menu text-center physicians-title " id="locations">
		    	ABOUT US: Locations		
		    </div>
		    <h4>Santa Ana</h4>
            <p>
            	Located 1 mile north of South Coast Plaza
            	<br>
            	Phone: +1 (714) 754-1684
            	<br>
            	2621 S. Bristol St., #108
            	Santa Ana, CA 92704
            </p>
            <br>
            <h4>Garden Grove</h4>
            <p>
            	Located 1 mile east of Harbor Blvd.
            	<br>
            	Phone: +1 (714) 638-2042
            	<br>
            	12665 Graden Grove Blvd., #204
            	Garden Grove, CA 92843
            </p>
		    <div class="row">
		    	<img class="banner-image" src="/images/mobile/about-conditions-mobile.jpg">
		    </div>
		    <div class="row bg-theme-brown home-menu text-center physicians-title " id="conditions">
		    	ABOUT US: Conditions We Treat		
		    </div>
		    <p class="informative-text">
		    	The Experienced cardiovascular specialists at Coastal Heart Medical Group treat nearly all types of Heart disease. But CHMG doesn't stop there. We treat many other heart and vascular conditions as well from most common to most complex, using only the most advanced treatments and technologies.
		    </p>
		    <h4>
		    	Some of the conditions we treat include:
		    </h4>
		    <ul>
		    	@foreach($conditions as $condition)
		    	<li>{{$condition['name']}}</li>
		    	@endforeach
		    </ul>
		</div>
	</div>
@endsection