@extends('mobile')

@section('content')
	<div class="page">
		<div class="container" style="">
            <div class="row">
		    	<img class="banner-image" src="/images/{{ $physician['mobile_avatar'] }}">
		    </div>
		    <div class="row bg-theme-brown home-menu text-center physicians-title ">
	    		{{$physician['name']}}
	    	</div>
	    	<p class="informative-text">
	    		{{$physician['bio']}}
	    		<a class="more-info-links" href="/physician/"><br> < Return to Previous Page</a>
	    	</p>
    	</div>
	</div>
@endsection