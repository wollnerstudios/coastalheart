@extends('mobile')

@section('content')
	<div class="page clinical-research">
		<div class="container" style="">
		    <div class="row">
		    	<img class="banner-image" src="/images/mobile/clinical-research-mobile.jpg">
		    </div>
		    <div class="row bg-theme-brown home-menu text-center">
	    		<h3>CLINICAL RESEARCH </h3>
	    	</div>
	    	<div class="row">
	    		<p class="informative-text">
	    			Our team of physicians has a combined experience of over 30 years in clinical research. The cardiovascular specialists at Coastal Heart Medical Group are all highly-trained, extremely experienced, and very passionate about their work and their patients. Our physicians treat nearly all types of heart disease and other heart and vascular conditions – from the most common to the most complex – using some of the most advanced treatments and technologies. Our facilities and level of care are designed to make sure we see you return as little as possible, if ever. We want your experience, and your outcome, to be the best ones you’ve ever received. At Coastal Heart Medical Group, our quality of care is higher so your quality of life can be better.
	    		</p>
	    	</div>
	    	
	    	<form class="coastal-form" action="">
	    		<div class="form-group">
					<label for= "first_name">First Name <span class="form-astrisk">*</span> </label>
					<input type="text" class="form-control" id= "first_name" placeholder="First Name" name= "first_name">
				</div>
				<div class="form-group">
					<label for="last_name">Last Name <span class="form-astrisk">*</span></label>
					<input type="text" class="form-control" id="last_name" placeholder="Last Name" name="last_name">
				</div>
				<div class="form-group">
					<label for="email">Email <span class="form-astrisk">*</span></label>
					<input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
				</div>
				<div class="form-group">
					<label for="phno">Phone Number <span class="form-astrisk">*</span></label>
					<input type="text" class="form-control"  name="phno" id="phno" placeholder="(###)###-####">
				</div>
				<div class="form-group">
					<label name="location" for="location-dropdown">Select Area of Interest <span class="form-astrisk">*</span></label>
					<select class="form-control">
						<option>Pick One </option>
						<option>1</option>
						<option>2</option>
					</select>
				</div>
				<div class="form-group news-letter-box" data-toggle="buttons">
					<label class="btn btn-warning text-center">
						<input type="checkbox" style="height: 30px;width: 30px;" autocomplete="off" name="news-letter">
						<span class="glyphicon glyphicon-ok"></span>
					</label>
					<span class="location-text">Yes! Send Me Coastal Heart Medical Group Updates.</span> 
				</div>
				

			
				<div class="text-center">
					<button type="submit" class="btn coastal-btn">Submit</button>
				</div>
				
			</form>
    	</div>
    	
	</div>
@endsection