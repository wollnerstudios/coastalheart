@extends('mobile')

@section('content')
	<div class="page page-physician">
		<div class="container" style="">
			@foreach($physicians as $physician)
	            <div class="row">
			    	<img class="banner-image" src="/images/{{ $physician['mobile_avatar'] }}">
			    </div>
			    <div class="row bg-theme-brown home-menu text-center physicians-title ">
		    		{{$physician['name']}}
		    	</div>
		    	<p class="informative-text" id="{{ $physician['slug'] }}-short">
		    		{!! $physician['shortBio'] !!}
		    		<a class="more-info-links" data-hide="{{ $physician['slug'] }}-short" data-show="{{ $physician['slug'] }}-long" href="/physician/{{ $physician['slug'] }}"><br>Click for more info ></a>
		    	</p>

		    	<p class="informative-text hidden-info" id="{{ $physician['slug'] }}-long">
		    		{!! $physician['bio'] !!}
		    		<a class="more-info-links" data-hide="{{ $physician['slug'] }}-long" data-show="{{ $physician['slug'] }}-short" href="/physician/{{ $physician['slug'] }}"><br>Click for less info ></a>
		    	</p>
	        @endforeach
    	</div>
	</div>
@endsection