@extends('mobile')

@section('content')
	<div class="page patients-forms-page">
		<div class="container" style="">
            <div class="row">
		    	<img class="banner-image" src="/images/mobile/patients-forms-banner-mobile.jpg">
		    </div>
		    <div class="row bg-theme-brown text-center">
		    	<h3>Patient Forms</h3>
		    </div>
		    <p class="informative-text">
		    	Please visit us on our desktop website to access all patient forms. On the full-site, you will be able to download and print any patient forms in both English and Spanish. Complete forms prior to your appointment to hep save your valueble time. We want to make your time at Coastal Heart Medical Group not just a pleasant one but easy and hassle-free as well.
		    </p>
		    <div class="row bg-theme-brown text-center">
		    	<h3>Make an Appointment</h3>
		    </div>
	    
            <h4 class="grey-text">Call us today to schedule your next appointment.</h4>
            <br>
            <h4>Santa Ana</h4>
            <p>
            	Located 1 mile north of South Coast Plaza
            	<br>
            	Phone: +1 (714) 754-1684
            	<br>
            	2621 S. Bristol St., #108
            	Santa Ana, CA 92704
            </p>
            <br>
            <h4>Garden Grove</h4>
            <p>
            	Located 1 mile east of Harbor Blvd.
            	<br>
            	Phone: +1 (714) 638-2042
            	<br>
            	12665 Graden Grove Blvd., #204
            	Garden Grove, CA 92843
            </p>
            <form class="coastal-form" action="">
	    		<div class="form-group">
					<label for= "first_name">First Name <span class="form-astrisk">*</span></label>
					<input type="text" class="form-control" id= "first_name" placeholder="First Name" name= "first_name">
				</div>
				<div class="form-group">
					<label for="last_name">Last Name <span class="form-astrisk">*</span></label>
					<input type="text" class="form-control" id="last_name" placeholder="Last Name" name="last_name">
				</div>
				<div class="form-group">
					<label for="email">Email <span class="form-astrisk">*</span></label>
					<input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
				</div>
				<div class="form-group">
					<label for="phno">Phone Number <span class="form-astrisk">*</span></label>
					<input type="text" class="form-control"  name="phno" id="phno" placeholder="(###)###-####">
				</div>
				<div class="form-group">
					<label for="location-dropdown">Select Location <span class="form-astrisk">*</span></label>
					<select class="form-control">
						<option>Pick One </option>
						<option>Santa Ana</option>
						<option>Garden Grove</option>
					</select>
				</div>
				<div class="form-group">
					<label for="comment">Additional Information:</label>
					<textarea class="form-control" rows="5" id="comment"></textarea>
				</div>
			
				<div class="text-center">
					<button type="submit" class="btn coastal-btn">Submit</button>
				</div>
				
			</form>
			<div class="row">
		    	<img class="banner-image" src="/images/mobile/rehab-center-mobile.jpg">
		    </div>
        	<div id="rehab-center" class="row bg-theme-brown text-center">
		    	<h3>Advice and Rehab Center</h3>
		    </div>
		    <p class="informative-text">
		    	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas ultricies augue ut dignissim. Nulla dapibus lectus ut iaculis eleifend. Phasellus nec fermentum neque. Cras id dapibus enim. Nam quis ligula aliquet, pretium libero a, lacinia nisi. Quisque rhoncus ultrices accumsan. Etiam rhoncus lorem erat, in luctus dui vehicula vel. Integer nunc mi, lacinia quis dignissim cursus, ultricies eget nulla. Pellentesque sagittis urna felis, a maximus neque pharetra non. Nulla faucibus scelerisque turpis, nec vehicula sem ornare non. Pellentesque eu erat in risus finibus pulvinar nec eget felis. Praesent laoreet orci erat, eget sollicitudin urna dignissim ac. Maecenas eget odio rutrum, rutrum ante id, convallis libero. Donec consectetur sapien eget est suscipit, nec scelerisque turpis porttitor.
		    </p>
    	</div>
	</div>
@endsection