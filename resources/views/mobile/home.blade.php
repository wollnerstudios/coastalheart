<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Coastal Heart Medical Group</title>
        @if(app()->isLocal())
            <link rel="stylesheet" href="{{ asset('css/mobile.css') }}">
        @else
            <link rel="stylesheet" href="{{ mix('css/mobile.css') }}">
        @endif
    </head>
    <body>


        <div class="page page-home">
            <div class="container">
                <nav id="navbar">
                    <div class="row">
                        <div class="col-xs-10">
                            <a href="/" class="logo-link">
                                <img class="mobile-logo logom" src="/images/home-logo.jpg">
                            </a>
                        </div>
                        <div class="col-xs-2 text-center nopadding">
                            <a href="tel:+17147541684"><span class="icons-home bg-theme-brown"><i class="fa fa-phone" aria-hidden="true"></i></span></a>
                            <a href="#"><span class="icons-home bg-theme-orange"><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                        </div>
                    </div>
                </nav>

                <div class="row">
                    <img class="main-img" src="/images/coastal-heart-building-logo.jpg">
                </div>

                <div class="row bg-theme-brown home-menu">
                    <a href="/physician" data-transition="flip">OUR PHYSICIANS
                        <span class="arrow-home"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                </div>

                <div class="row bg-theme-orange home-menu">
                    <a href="/about">ABOUT US
                        <span class="arrow-home"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                </div>

                {{--<div class="row bg-theme-red home-menu">--}}
                {{--<a href="/patients">PATIENT PORTAL <span class="arrow-home"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>--}}
                {{--</div>--}}

                <div class="row bg-theme-brown home-menu">
                    <a href="/services">HEART & VASCULAR SERVICES
                        <span class="arrow-home"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                </div>

                <div class="row bg-theme-orange home-menu">
                    <a href="/clinical-research">CLINICAL RESEARCH
                        <span class="arrow-home"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                </div>

                <br>

                <div class="row">
                    <br>
                    <div class="col-sm-12 text-center">
                        <img src="/images/chmg-final-logo_360.jpg">
                    </div>
                    <br>
                    <div class="col-sm-12 text-center address-home">
                        Coastal Heart Medical Group, Santa Ana<br>2621 S. Bristol St., #108,<br>Santa Ana, CA 92704<br><a href="tel:7147541684">Phone: +1 (714) 754-1684</a>
                    </div>
                    <br>
                    <div class="col-sm-12 text-center address-home">
                        Coastal Heart Medical Group, Garden Grove<br>12665 Garden Grove Blvd., #203,<br>Garden Grove, CA 92843<br><a href="tel:7146382042">Phone: +1 (714) 638-2042</a>
                    </div>
                    <br>
                    <div class="col-sm-12 text-center">
                        <a href="https://www.facebook.com/coastalhmg/"><i class="fa fa-facebook fa-2x"></i></a>
                    </div>
                    <br>
                    <div class="col-sm-12 text-center">
                        © Copyright Coastal Heart Medical Group, Inc. <br>Privacy Policy
                    </div>
                    <div class="col-sm-12 text-center">
                        <a href="https://wbrandstudio.com/" class="wbrand-link" target="_blank">Site development by W Brand Studio</a>
                    </div>
                    <br>
                </div>
            </div>
        </div>

        @include('mobile.partials.menu')

        @if(app()->isLocal())
            <script src="{{ asset('js/app.js') }}"></script>
        @else
            <script src="{{ mix('js/app.js') }}"></script>
        @endif
    </body>
</html>
