<div id="menu">
    <a href="" id="menu-close-button"><i class="fa fa-close fa-2x"></i></a>
    <ul class="main-links">
        <li><a href="/physician">OUR PHYSICIANS</a></li>
        <li>
            <a href="/about">ABOUT US</a>
            <ul class="children-links">
                <li><a href="/about#locations">Locations</a></li>
                <li><a href="/about#conditions">Conditions We Treat</a></li>
                <li><a href="/about#staff">Staff</a></li>
            </ul>
        </li>
        <li>
            <a href="/services">HEART & VASCULAR SERVICES</a>
            <ul class="children-links">
                <li><a href="/services#angiography-facilities">Angiography Facilities</a></li>
                <li><a href="/services#diagnostic-testing">Diagnostic Testing</a></li>
                <li><a href="/services#venous-studies-treatments">Venous Studies Treatments</a></li>
            </ul>
        </li>
        <li><a href="/clinical-research">CLINICAL RESEARCH</a></li>
    </ul>
</div>
