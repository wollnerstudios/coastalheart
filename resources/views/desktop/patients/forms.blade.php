@extends('desktop')

@section('content')
    <div class="page page-patients-forms max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'Patient Forms'])

            <div class="row row-bottom">
                <div class="col-xs-12 col-sm-6 no-padding max-height">
                    <div class="main-image-container animated slide-in-right"></div>
                    <div class="sub-title-container bg-theme-brown vertical-center-content animated slide-in-up">
                        <h2 class="no-margin text-center">Patient Care and Advice</h2>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 main-content-container max-height vertical-center-content animated fade-in-up">
                    <div>
                        <p>We want to make your experience at Coastal Heart Medical Group not just a pleasant one, but easy and hassle-free as well. We know your time is valuable, that’s why we provide NEW PATIENT forms online, which you can complete prior to your appointment. One less thing, right? It can be easy to forget important details when filling out forms in the office. So, now you can complete your forms SECURELY online, in the comfort of your home, at a convenient time when you have access to all your medical information.</p>
                        <p>Once you have filled out your form, you can then set up an appointment. Now isn’t that easy?</p>

                        {{--<div class="secure-forms-container row">--}}
                            {{--<div class="col-xs-8 vertical-center-content">--}}
                                {{--<div>--}}
                                    {{--<p>CHOOSE A SECURE VERSION BELOW TO FILL OUT THE NEW PATIENT FORM ONLINE.</p>--}}

                                    {{--<div class="row text-center">--}}
                                        {{--<div class="col-xs-6">--}}
                                            {{--<img src="/images/flags_03.jpg" alt="" class="flag"><br>--}}
                                            {{--<a href="/forms/patient-form-ENGLISH.pdf" download>English Version</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-xs-6">--}}
                                            {{--<img src="/images/flags_05.jpg" alt="" class="flag"><br>--}}
                                            {{--<a href="/forms/patient-form-SPANISH.pdf" download>Spanish Version</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-4 right-col"></div>--}}
                        {{--</div>--}}

                        <div class="row pdf-icons-container">
                            <div class="col-xs-6">
                                <a href="/forms/patient-form-ENGLISH.pdf" target="_blank">
                                    <img class="pdf-icon img-responsive" src="/images/english-pdf.jpg" alt="">
                                </a>
                                <a href="/forms/patient-form-SPANISH.pdf" target="_blank">
                                    <img class="pdf-icon img-responsive" src="/images/spanish-pdf.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-xs-6">
                                <p>
                                    Download a PDF form, print it out and bring it to the office. Just click on
                                    the preferred language version to download.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
