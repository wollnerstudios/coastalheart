@extends('desktop')

@section('content')
    <div class="page page-physician max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => $physician['name']])

            <div class="row row-bottom" style="overflow: hidden;">
                <div class="col-xs-12 col-sm-6 no-padding max-height animated slide-in-up">
                    <div class="main-image-container" style="background-image: url(/images/{{ $physician['avatar'] }})"></div>
                    @include('desktop.partials.sub-navigation', compact('physician'))
                </div>
                <div class="col-xs-12 col-sm-6 description-container">
                    <div class="vertical-center-content max-height">
                        <div>
                            <article class="animated fade-in-up">{!! $physician['bio'] !!}</article>
                        </div>
                    </div>
                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
