@extends('desktop')

@section('content')
    <div class="page page-clinical-research max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'Clinical Research'])

            <div class="row row-bottom">
                <div class="col-xs-6 max-height no-padding animated slide-in-right">
                    <div class="description-container bg-theme-orange">
                        <article class="max-height">
                            <div class="vertical-center-content max-height">
                                <div>
                                    <p>In clinic and hospital settings, research trials are performed with patients who generously give physicians, and those in the cardiovascular research and healthcare industry, the opportunity to further study cardiovascular disease. The cardiologists at Coastal Heart Medical Group have been active in research for over 20 years, advancing new prevention and treatment options for our patients. These studies are vital and allow us to provide innovative treatments to our patients who are involved in these investigative trials. We are grateful to our patients who take part in them.</p>

                                    <p><strong>​Why do we participate in clinical trials?</strong></p>

                                    <p>Our physicians believe that by studying new medications and treatment options for cardiovascular disease, they will be able to offer progressive medical care to their patients.</p>

                                    <p><strong>​What criteria must the patient meet to participate in a trial?</strong></p>

                                    <p>Each clinical study has specific criteria which the patient must meet. These guidelines relate to the patient’s condition and determine whether he/she is qualified to participate. If considering whether to participate in a study or not, it is best to speak with your physician about any concerns or questions you may have.</p>

                                    <p>If you have a heart-related condition and would like to participate in a trial, speak with your Coastal Heart Medical Group cardiologist first.</p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="bg-theme-red" style="height: 100px;"></div>
                </div>
                <div class="col-xs-6 no-padding max-height">
                    <div class="main-image-container max-height animated slide-in-up"></div>
                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
