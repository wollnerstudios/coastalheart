@extends('desktop')

@section('content')
    <div class="page page-contact max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'Contact'])

            <div class="row row-bottom">
                <div class="col-xs-6 max-height vertical-center-content">
                    <form action="">
                        <div class="form-group row">
                            <label for="name" class="col-xs-3 col-form-label text-right">Name</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-xs-3 col-form-label text-right">Email</label>
                            <div class="col-xs-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="number" class="col-xs-3 col-form-label text-right">Phone Number</label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="number" name="number" placeholder="Phone Number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-xs-3 col-form-label text-right">Message</label>
                            <div class="col-xs-9">
                                <textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
                            </div>
                        </div>

                        <div class="text-right">
                            <button class="btn bg-theme-orange">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
