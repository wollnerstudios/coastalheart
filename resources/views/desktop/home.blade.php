@extends('desktop')

@section('content')
    <div itemscope itemtype="http://schema.org/LocalBusiness" class="page page-home">
        <div class="container-fluid">
            <div class="row top-level">
                <div class="col-xs-5 bg-theme-orange max-height vertical-center-content text-center box-short-description animated slide-in-right">
                    <div>
                        <h4 class="text-uppercase">Coastal Heart Medical Group</h4>

                        <p>Designed for patients with cardiac and vascular conditions</p>

                        <a href="https://www.facebook.com/coastalhmg/"><i class="fa fa-facebook fa-2x"></i></a>
                    </div>
                </div>
                <div class="col-xs-7 bg-theme-red max-height animated slide-in-left">
                    <div class="row max-height">
                        <div class="col-xs-1 max-height vertical-center-content accredited-img">
                            <img src="images/accrediation.png" alt="radiology accredited medal">
                        </div>
                        <div class="col-xs-6 col-xs-offset-5 max-height vertical-center-content contact-information">
                            <p>
                                2621 S. Bristol St., #108, Santa Ana, CA 92704<br>
                                12665 Garden Grove Blvd., #204, Garden Grove, CA 92843<br>
                                Office: (714) 754-1684, Fax: (714) 966-0417
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row top-level height-2">
                <div class="col-xs-6 max-height vertical-center-content logo-container animated slide-in-right">
                    <img itemprop="image" src="/images/home-logo.jpg" alt="" class="img-responsive center-block">
                </div>
                <div class="col-xs-6 max-height animated slide-in-left">
                    <div class="row max-height">
                        <div class="col-xs-12 half-height bg-theme-brown">
                            <div class="row max-height">
                                <div class="col-xs-7 max-height text-center vertical-center-content">
                                    <h2 class="text-uppercase">About Us</h2>
                                </div>
                                <div class="col-xs-5 max-height vertical-center-content">
                                    <ul class="links-list">
                                        <li><a class="animate-page-change" href="/about/locations">Locations</a></li>
                                        <li><a class="animate-page-change" href="/about/conditions-we-treat">Conditions We Treat</a></li>
                                        <li><a class="animate-page-change" href="/about/staff">Staff</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 half-height bg-theme-orange">
                            <div class="row max-height">
                                <div class="col-xs-7 max-height text-center vertical-center-content">
                                    <h2 class="text-uppercase">Patient Portal</h2>
                                </div>
                                <div class="col-xs-5 max-height vertical-center-content">
                                    <ul class="links-list">
                                        <li><a class="animate-page-change" href="/patients/forms">Patient Forms</a></li>
                                        <li><a class="animate-page-change" href="/about/locations">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row top-level height-2">
                <div class="col-xs-7 max-height bg-theme-red animated slide-in-right">
                    <div class="row max-height">
                        <div class="col-xs-6 max-height text-center vertical-center-content">
                            <h2 class="text-uppercase">Our Physicians</h2>
                        </div>
                        <div class="col-xs-6 max-height vertical-center-content">
                            <ul class="links-list physicians-list">
                                @foreach($physicians as $physician)
                                    <li>
                                        <a class="animate-page-change" href="/physician/{{ $physician['slug'] }}" id="{{ $physician['slug'] }}">{{ $physician['name'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5 max-height building-image-container home-page-slick main-image-container animated slide-in-left">
                    <div class="main-image main-image--main"></div>
                    <div class="main-image main-image--one"></div>
                    <div class="main-image main-image--two"></div>
                    <div class="main-image main-image--three"></div>
                    <div class="main-image main-image--four"></div>
                    <div class="main-image main-image--five"></div>
                    <div class="main-image main-image--six"></div>
                    <div class="main-image main-image--seven"></div>
                </div>
            </div>

            <div class="row top-level">
                <div class="col-xs-4 max-height bg-theme-orange clinical-research-container animated slide-in-right">
                    <div class="row max-height">
                        <div class="col-xs-12">
                            <h2 class="text-uppercase">Clinical Research</h2>

                            <p>
                                Our team of physicians has a combined experience of over 30 years in clinical research.
                                <a class="animate-page-change" href="/clinical-research">Read More <i class="fa fa-chevron-right"></i></a>
                            </p>
                        </div>
                        
                    </div>
                </div>
                <div class="col-xs-8 max-height bg-theme-brown animated slide-in-left">
                    <div class="row max-height">
                        <div class="col-xs-7 max-height text-center vertical-center-content">
                            <h2 class="text-uppercase">Heart & Vascular Services</h2>
                        </div>
                        <div class="col-xs-5 max-height vertical-center-content">
                            <ul class="links-list">
                                <li><a class="animate-page-change" href="/services/angiography-facilities">Angiography Facilities</a></li>
                                <li><a class="animate-page-change" href="/services/venous-studies-treatments">Venous Studies & Treatments</a></li>
                                <li><a class="animate-page-change" href="/services/diagnostic-testing">Non-invasive [Diagnostic] Testing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<meta itemprop="streetAddress" content="2621 S Bristol St # 108">
<meta itemprop="addressLocality" content="Santa Ana">
<meta itemprop="addressRegion" content="California">
<meta itemprop="addressCountry" content="US">
<meta itemprop="postalCode" content="92704"></span>
        <meta itemprop="name" content="Coastal Heart Medical Group">
        <span itemprop="openingHoursSpecification" itemscope itemtype="http://schema.org/OpeningHoursSpecification">
<span itemprop="dayOfWeek" itemscope itemtype="http://schema.org/DayOfWeek">
<meta itemprop="name" content="Mon-Fri 9:00AM-5:30PM"></span></span>
        <meta itemprop="url" content="https://coastalheart.org/">
        <meta itemprop="telephone" content="(714) 754-1684"></div>
@endsection
