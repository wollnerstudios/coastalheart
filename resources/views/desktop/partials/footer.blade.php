<footer id="footer" class="animated fade-in-up">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-5 text-right">
                <p>
                    Coastal Heart Medical Group, Santa Ana<br>
                    2621 S. Bristol St., #108<br>
                    Santa Ana, CA 92704<br>
                    Phone: +1 (714) 754-1684
                </p>
            </div>
            <div class="col-xs-2 text-center">
                <a href="/" class="animate-page-change">
                    <img src="/images/home-logo.jpg" alt="Coastal Heart Medical Group Logo" class="logo img-responsive">
                </a>
            </div>
            <div class="col-xs-5">
                <p>
                    Coastal Heart Medical Group, Garden Grove<br>
                    12665 Garden Grove Blvd., #203<br>
                    Garden Grove, CA 92843<br>
                    Phone: +1 (714) 638-2042
                </p>
            </div>
            <div class="col-xs-12 text-center">
                <a href="https://www.facebook.com/coastalhmg/"><i class="fa fa-facebook fa-2x"></i></a>
                <p>
                    &copy; Copyright {{ date('Y') }} Coastal Heart Medical Group, Inc.<br>
                    Website and Branding by <a href="https://wbrandstudio.com">W Brand Studio</a>.
                </p>
            </div>
        </div>
    </div>
</footer>
