<div class="bg-theme-red animated slide-in-up">
    <div class="row sub-nav">
        <a href="/services/angiography-facilities" class="col-xs-4 animate-page-change">Angiography</a>
        <a href="/services/venous-studies-treatments" class="col-xs-4 animate-page-change">Venous Studies</a>
        <a href="/services/diagnostic-testing" class="col-xs-4 animate-page-change">Diagnostic Testing</a>
    </div>
</div>