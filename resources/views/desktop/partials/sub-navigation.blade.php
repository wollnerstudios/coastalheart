<div class="sub-navigation-container bg-theme-{{ $theme ?? 'orange' }}">
    <div class="row">
        <a href="/physician/{{ $prevPhysician['slug'] }}" class="col-xs-3 animate-page-change text-right text-uppercase">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            prev
        </a>
        <div class="col-xs-6 text-center">
            <h4 class="no-margin">{{ $physician['name'] }}</h4>
        </div>
        <a href="/physician/{{ $nextPhysician['slug'] }}" class="col-xs-3 animate-page-change text-uppercase" (click)="nextItem($event)">
            next
            <i class="fa fa-caret-right" aria-hidden="true"></i>
        </a>
    </div>
</div>
