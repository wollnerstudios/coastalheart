<!-- Modal -->


<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form action="#" id="contact-form" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: #fff">&times;</span></button>
                <h4 class="modal-title">Contact</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="sr-only">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="email" class="sr-only">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="phone_number" class="sr-only">Phone Number</label>
                    <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number">
                </div>
                <div class="form-group">
                    <p>Location:</p>
                    <label class="radio-inline">
                        <input type="radio" name="location" id="location-radio-1" value="Santa Ana"> Santa Ana
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="location" id="location-radio-2" value="Garden Grove"> Garden Grove
                    </label>
                </div>
                <div class="form-group">
                    <label for="body" class="sr-only">Message</label>
                    <textarea class="form-control" id="body" name="body" placeholder="Message" rows="4"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" onClick="ga('send', 'event', { eventCategory: 'Form', eventAction: 'Submit', eventLabel: 'Contact', eventValue: 1});">Submit</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="contact-submission-success-modal" id="contact-submission-success-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: #fff">&times;</span></button>
                <h4 class="modal-title">Contact Form Submitted</h4>
            </div>
            <div class="modal-body">
                <p>Thanks for contacting us! We'll get back to you as soon as possible!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>