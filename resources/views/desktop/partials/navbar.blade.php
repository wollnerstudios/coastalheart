<nav id="navbar" class="row row-top nav-bar">
    <div class="col-xs-6 max-height no-padding animated slide-in-down">
        <a class="logo-container max-height vertical-center-content animate-page-change" href="/">
            <img src="/images/home-logo.jpg" alt="Coastal Heart Medical Group Logo" class="logo max-height">
        </a>
        <a href="#" class="contact-link" data-toggle="modal" data-target="#contact-modal">
            <img src="/images/envelope-icon_03.jpg" alt="">
        </a>
        <a class="menu-button bg-theme-red text-center animate-page-change" href="/">
            <span>menu</span>
        </a>
    </div>
    <div class="col-xs-6 bg-theme-brown max-height text-center vertical-center-content animated slide-in-left">
        <h1 class="no-margin">{{ $title }}</h1>
    </div>
</nav>
