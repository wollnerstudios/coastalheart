@extends('desktop')

@section('content')
    <div class="page page-about-conditions max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'ABOUT US: Conditions We Treat'])

            <div class="row row-bottom">
                <div class="col-xs-6 no-padding max-height">
                    <div class="main-image-container animated slide-in-right"></div>
                    <div class="sub-navigation bg-theme-orange animated slide-in-up" style="height: 100px;">
                        <div class="row text-center">
                            <a href="/about/locations" class="col-xs-4 animate-page-change">Locations</a>
                            <a href="/about/conditions-we-treat" class="col-xs-4 animate-page-change">Conditions We Treat</a>
                            <a href="/about/staff" class="col-xs-4 animate-page-change">Staff</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 description-container">
                    <article class="max-height animated fade-in-up">
                        <div class="vertical-center-content max-height">
                            <div>
                                <p>
                                    The experienced cardiovascular specialists at Coastal Heart Medical Group treat nearly
                                    all types of heart disease. But CHMG doesn’t stop there. We treat many other heart and
                                    vascular conditions as well, from the most common to the most complex, using only the
                                    most advanced treatments and technologies.
                                </p>

                                <h4 class="text-theme-brown">Some of the conditions we treat include:</h4>

                                <div class="row">
                                    @foreach($conditions->chunk(($conditions->count() / 2) + 1) as $chunk)
                                        <div class="col-xs-6">
                                            <ul>
                                                @foreach($chunk as $condition)
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-target="#condition-modal-{{ str_slug($condition['name']) }}">{{ $condition['name'] }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>

    @foreach($conditions as $condition)
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
             id="condition-modal-{{ str_slug($condition['name']) }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-theme-orange">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: #fff">&times;</span></button>
                        <h4 class="modal-title text-center">{{ $condition['name'] }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>{{ $condition['description'] }}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
