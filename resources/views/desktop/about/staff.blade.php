@extends('desktop')

@section('content')
    <div class="page page-about-staff max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'ABOUT US: Staff'])

            <div class="row row-bottom">
                <div class="col-xs-6 no-padding max-height">
                    <div class="main-image-container animated slide-in-right"></div>
                    <div class="sub-navigation bg-theme-orange animated slide-in-up" style="height: 100px;">
                        <div class="row text-center">
                            <a href="/about/locations" class="col-xs-4 animatePageChange">Locations</a>
                            <a href="/about/conditions-we-treat" class="col-xs-4 animatePageChange">Conditions We Treat</a>
                            <a href="/about/staff" class="col-xs-4 animatePageChange">Staff</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 description-container max-height">
                    <div class="vertical-center-content max-height">
                        <div>
                            <article class="animated fade-in-up">
                                <p>
                                    Our winning team is the coastal heart medical group leader in cardiovascular health. For more than 30 years we’ve provided the highest level of care utilizing the latest technology.  All of our physicians are Board Certified in Cardiovascular Diseases and Internal Medicine. They are trained and qualified in every aspect of non-invasive, invasive and interventional cardiology.
                                </p>

                                <p>
                                    Top left: Mercedeh Esmaeli, NP, MSN, RN
                                    - Salem A. Sbaity, MD
                                    - Anil V. Shah, MD, FACC
                                    - Tracy Aresenault, PA-C
                                    - Amer R. Zarka, MD, FACC
                                    - Mohammad A. Hajjar, MD
                                </p>

                                <p>
                                    Bottom picture: our wonderful and dedicated staff at the Santa Ana office.
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
