@extends('desktop')

@section('content')
    <div class="page page-about-locations max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'ABOUT US: Locations'])

            <div class="row row-bottom">
                <div class="col-xs-6 left-image animated slide-in-right"></div>
                <div class="col-xs-6 right-image animated slide-in-left"></div>

                <div class="col-xs-6 left-col-text animated slide-in-right medalLogo--left">
                    <div>
                        <p>
                            <strong class="text-theme-brown">Santa Ana</strong><br>
                            Located 1 mile north of South Coast Plaza<br>
                            Phone: +1 (714) 754-1684
                        </p>

                        <p>
                            2621 S. Bristol St., #108<br>
                            Santa Ana, CA 92704
                        </p>
                    </div>
                </div>
                <div class="col-xs-6 right-col-text animated slide-in-left  medalLogo--right">
                    <div>
                        <p>
                            <strong class="text-theme-brown">Garden Grove</strong><br>
                            Located 1 mile east of Harbor Blvd.<br>
                            Phone: +1 (714) 638-2042
                        </p>

                        <p>
                            12665 Garden Grove Blvd., #203<br>
                            Garden Grove, CA 92843
                        </p>
                    </div>
                </div>

                <a href="#" class="col-xs-12 questions-button bg-theme-orange text-center animated slide-in-up" data-toggle="modal" data-target="#contact-modal">
                    Questions? Contact either location by clicking here.
                </a>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
