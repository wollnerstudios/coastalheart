@extends('desktop')

@section('content')
    <div class="page page-services-diagnostic-testing max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'Services: Diagnostic Testing'])

            <div class="row row-bottom">
                <div class="col-xs-12 col-sm-6 no-padding left-col">
                    <article class="description-container bg-theme-orange vertical-center-content animated slide-in-right">
                        <div>
                            <p>
                                Our facilities in Orange County, CA are meticulously designed with state-of-the-art equipment and instruments and include a wide range of non-invasive cardiovascular capabilities. The experienced cardiologists on staff use nothing but the latest technology to detect heart disease and discover potential problems in order to arrive at the perfect solutions and begin treatment as soon as possible.
                            </p>

                            <ul role="tablist">
                                <li>Nuclear testing</li>
                                <li>Electrocardiograms</li>
                                <li>Treadmill exercise stress testing</li>
                                <li>Cardiac and peripheral ultra-sound imaging</li>
                                <li>Heart monitors</li>
                                <li>Ultrasound testing with exercise</li>
                                <li>Transesophageal imaging</li>
                                <li>Pacemaker assessments</li>
                                <li>Pharmaceutical stress testing</li>
                            </ul>
                        </div>
                    </article>

                    @include('desktop.partials.sub-navigation-services')
                </div>

                <div class="col-xs-12 col-sm-6 main-image-container max-height animated slide-in-up"></div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
