@extends('desktop')

@section('content')
    <div class="page page-services-venous-studies-treatments max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'Services: Venous Studies & Treatments'])

            <div class="row row-bottom">
                <div class="col-xs-12 col-sm-6 no-padding left-col">
                    <article class="description-container bg-theme-orange vertical-center-content animated slide-in-right ">
                        <div>
                            <p>
                                We perform many venous studies at Coastal Heart Medical Group. Having an advanced vascular
                                diagnostic and treatment center in-house, we are able to detect the location and severity of
                                venous and arterial vascular diseases quicker so we may treat it faster. These are
                                state-of-the-art, cost-effective and painless procedures, performed by our cardiologists who
                                have credentials in vascular angiography medicine and vascular intervention. Plus, we are
                                proud to announce that we just opened our own interventional suite to treat varicose diseases.
                            </p>

                            <h4>Treatments and procedures include:</h4>

                            <ul>
                                @foreach($treatments as $treatment)
                                    <li>
                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                           href="#treatment-{{ str_slug($treatment['name']) }}" aria-expanded="true" aria-controls="treatment-{{ str_slug($treatment['name']) }}">
                                            {{ $treatment['name'] }} (CLICK FOR MORE INFO)
                                        </a>
                                        <div id="treatment-{{ str_slug($treatment['name']) }}" class="panel-collapse collapse \\" role="tabpanel" aria-labelledby="headingOne">
                                            {!! $treatment['description'] !!}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </article>

                    @include('desktop.partials.sub-navigation-services')
                </div>

                {{--<div class="col-xs-12 col-sm-6 main-image-container max-height animated slide-in-up"></div>--}}
                <div class="col-xs-12 col-sm-6 max-height ven-slide main-image-container animated slide-in-left">
                    <div class="main-image main-image--ten"></div>
                    <div class="main-image main-image--eight"></div>
                    <div class="main-image main-image--nine"></div>

                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
