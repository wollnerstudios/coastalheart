@extends('desktop')

@section('content')
    <div class="page page-physician max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'SPIDER VEIN TREATMENT'])

            <div class="row row-bottom">
                <div class="col-xs-12 col-sm-6 no-padding max-height animated slide-in-up">
                    <div class="spider-treatment-container">
                        <div class="spider-treatment-img spider-treatment-img--one"></div>
                        <div class="spider-treatment-img spider-treatment-img--two"></div>
                    </div>
                    {{--<article class="description-container bg-theme-orange vertical-center-content animated slide-in-right">--}}
                    {{--<div>--}}
                    {{--<p>--}}
                    {{--We want to provide our patients with more choices and more solutions – the kind they can’t get anywhere else. That’s why Coastal Heart Medical Group is one of the few in Orange County, CA with its own in-house Angiography Facilities. The facility can perform and diagnose arterial and venous narrowing. We offer a variety of procedures including balloon angioplasty, stent placement and atherectomy.--}}
                    {{--</p>--}}

                    {{--<h4>Our services include:</h4>--}}

                    {{--<ul>--}}
                    {{--<li>Identify narrowed, enlarged, and blocked blood vessels</li>--}}
                    {{--<li>Identify blocked and hardened</li>--}}
                    {{--<li>Test for blood clots</li>--}}
                    {{--<li>Venous ablation</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</article>--}}

                    @include('desktop.partials.sub-navigation-services')
                </div>

                <div class="col-xs-12 col-sm-6 description-container spider-treatment--description">
                    <div class="vertical-center-content max-height">
                        <article class="animated fade-in-up">
                            <div>
                                <p>
                                    If you suffer from varicose veins, there are a variety of treatment options
                                    available to you at Coastal Heart Medical Group. We are a leading vein removal
                                    clinic in Orange County specialized in all kinds of vein removal procedures. We
                                    perform effective and safe procedures for reducing spider veins. Spider veins may
                                    also be treated with a laser, which which obliterates it through the skin.
                                    Sometimes, laser therapy is used in combination with sclerotherapy.
                                </p>

                                <p>
                                    The most common spider vein treatment involves Sclerotherapy, which is a medical
                                    procedure used to eliminate varicose veins and spider veins. Sclerotherapy involves
                                    an injection of a solution directly into the vein. The solution causes the vein to
                                    scar, forcing blood to reroute through healthier veins.
                                </p>
                            </div>
                        </article>
                    </div>
                </div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
