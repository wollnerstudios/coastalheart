@extends('desktop')

@section('content')
    <div class="page page-services-angiography-facilities max-height">
        <div class="container-fluid max-height">
            @include('desktop.partials.navbar', ['title' => 'Services: Angiography Facilities'])

            <div class="row row-bottom">
                <div class="col-xs-12 col-sm-6 no-padding left-col">
                    <article class="description-container bg-theme-orange vertical-center-content animated slide-in-right">
                        <div>
                            <p>
                                We want to provide our patients with more choices and more solutions – the kind they can’t get anywhere else. That’s why Coastal Heart Medical Group is one of the few in Orange County, CA with its own in-house Angiography Facilities. The facility can perform and diagnose arterial and venous narrowing. We offer a variety of procedures including balloon angioplasty, stent placement and atherectomy.
                            </p>

                            <h4>Our services include:</h4>

                            <ul>
                                <li>Identify narrowed, enlarged, and blocked blood vessels</li>
                                <li>Identify blocked and hardened</li>
                                <li>Test for blood clots</li>
                                <li>Venous ablation</li>
                            </ul>
                        </div>
                    </article>

                    @include('desktop.partials.sub-navigation-services')
                </div>

                <div class="col-xs-12 col-sm-6 main-image-container max-height animated slide-in-up"></div>
            </div>

            @include('desktop.partials.footer')
        </div>
    </div>
@endsection
