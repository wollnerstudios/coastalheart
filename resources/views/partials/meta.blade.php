<title>{{ $meta['title'] ?? 'Coastal Heart Medical Group | Cardiac & Vascular Treatments' }}</title>

<meta name="description" content="{{ $meta['description'] ?? 'For over 30 years, our team of cardiovascular specialists have treat nearly all types of heart disease using the most advanced treatments and technologies.' }}">
<meta name="keywords" content="{{ $meta['keywords'] ?? 'medical group, sclerotherapy, varicose veins treatment ' }}">
<meta name="author" content="{{ $meta['author'] ?? 'Coastal Heart Medical Group' }}">
<meta name="robots" content="{{ $meta['robots'] ?? 'all' }}">
<meta name="robots" content="{{ $meta['robots'] ?? 'index, follow' }}">
<meta name="revisit-after" content="{{ $meta['revisit-after'] ?? '4 days' }}">
<link rel="publisher" href="{{ $meta['publisher'] ?? 'https://wbrandstudio.com/' }}">


