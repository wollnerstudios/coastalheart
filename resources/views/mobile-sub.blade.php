<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Coastal Heart Medical Group</title>

        @if(app()->isLocal())
            <link rel="stylesheet" href="{{ asset('css/mobile.css') }}">
        @else
            <link rel="stylesheet" href="{{ mix('css/mobile.css') }}">
        @endif
    </head>
    <body>
        <nav id="navbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-9">
                        <a href="/" class="logo-link">
                            <img class="mobile-logo logom" src="/images/home-logo.jpg">
                        </a>
                    </div>
                    <div class="col-xs-3 text-center nopadding">
                        <span class="bg-theme-brown menu-box" style="">
                            <a>menu</a>
                        </span>
                    </div>
                </div>
            </div>
        </nav>

        @yield('content')
        <div class="row bg-theme-red">
            <br>
            <div class="col-sm-12 text-center">
                2621 S. Bristol St., #108,<br>Santa Ana, CA 92704
            </div>
            <br>
            <div class="col-sm-12 text-center">
                12665 Garden Grove Blvd., #204,<br>Garden Grove, CA 92843
            </div>
            <br>
            <div class="col-sm-12 text-center">
                <a href="https://www.facebook.com/coastalhmg/"><i class="fa fa-facebook fa-2x"></i></a>
            </div>
            <br>
            <div class="col-sm-12 text-center footer-privacy-text">
                © Copyright Coastal Heart Medical Group, Inc.
            </div>
            <div class="col-sm-12 text-center footer-privacy-text">
                Privacy Policy <br>Site development by W Brand Studio
            </div>
        </div>

        @include('mobile.partials.menu')

        @if(app()->isLocal())
            <script src="{{ asset('js/app.js') }}"></script>
        @else
            <script src="{{ mix('js/app.js') }}"></script>
        @endif
    </body>
</html>
