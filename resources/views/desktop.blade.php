<!doctype html>
<html lang="en">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-PJF2HVL');</script>
        <!-- End Google Tag Manager -->


        <!-- Google Verification -->
        <meta name="google-site-verification" content="h28s507XH1jTQTySMpkFgaF6wcC1qiyRQK83CO_pwAM" />

        <!-- Bing Verifcation -->
        <meta name="msvalidate.01" content="CB945FD7AF5FE99662DD9CE14EB4CFB1" /> 


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">


        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="stylesheet" href="vendor/slick.css">
        <link rel="stylesheet" href="../vendor/slick.css">
        <meta name="theme-color" content="#ffffff">

        <meta name=“distribution” content="Global" />
        <meta name=“location” content="Santa Ana, Orange County, California" />
        <meta name=“location” content="OC, LA, CA" />
        <meta name="abstract" content="santa ana coastal heart medical group, santa ana medical group, santa ana health care medical group, santa ana healthcare medical group, santa ana healthcare urgent care, santa ana cardiovascular specialists, orange county medical group, orange county" />

        <meta name="geo.region" content="US-CA" />
        <meta name="geo.placename" content="Santa Ana" />
        <meta name="geo.position" content="33.711874;-117.884667" />
        <meta name="ICBM" content="33.711874, -117.884667" />


        <meta name=“distribution” content="Global" />
        <meta name=“location” content="Garden Grove, Orange County, California" />
        <meta name=“location” content="OC, LA, CA" />
        <meta name="abstract" content="garden grove coastal heart medical group, garden grove medical group, garden grove health care medical group, garden grove healthcare medical group, garden grove healthcare urgent care, garden grove cardiovascular specialists, orange county medical group, orange county" />

        <meta name="geo.region" content="US-CA" />
        <meta name="geo.placename" content="Garden Grove" />
        <meta name="geo.position" content="33.775314;-117.912176" />
        <meta name="ICBM" content="33.775314, -117.912176" />



        @include('partials.meta')

        @if(app()->isLocal())
            <link rel="stylesheet" href="{{ asset('css/desktop.css') }}">
            <link rel="stylesheet" href="{{ asset('vendor/slick.css') }}">
        @else
            <link rel="stylesheet" href="{{ mix('css/desktop.css') }}">
            <link rel="stylesheet" href="{{ asset('vendor/slick.css') }}">
        @endif

        <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-105966207-1', 'auto');
        ga('send', 'pageview');
        </script>
        <script>
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            ga('send', 'event', 'Contact Form', 'submit');
        }, false );
        </script>
    </head>
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJF2HVL"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        @yield('content')

        @include('desktop.partials.modals')

        @if(app()->isLocal())
            <script src="{{ asset('js/app.js') }}"></script>
            <script src="{{ asset('vendor/slick.min.js') }}"></script>
        @else
            <script src="{{ mix('js/app.js') }}"></script>
            <script src="{{ asset('vendor/slick.min.js') }}"></script>
        @endif

<<<<<<< HEAD
=======
    <script type="text/javascript" src="vendor/slick.min.js"></script>
        <script type="text/javascript" src="../vendor/slick.min.js"></script>
>>>>>>> 61d76b224289663fdec8796682e88fac3bc5d454
    </body>
</html>
