<!doctype html>
<html lang="en">
    <head>


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Coastal Heart Medical Group</title>

        @if(app()->isLocal())
            <link rel="stylesheet" href="{{ asset('css/mobile.css') }}">
        @else
            <link rel="stylesheet" href="{{ mix('css/mobile.css') }}">
        @endif

        <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-105966207-1', 'auto');
        ga('send', 'pageview');
        </script>
    </head>
    <body>

        <nav id="navbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-9">
                        <a href="/" class="logo-link">
                            <img class="logom" src="/images/home-logo.jpg">
                        </a>
                    </div>
                    <div class="col-xs-3 text-center nopadding">
                        <span class="bg-theme-brown menu-box" style="">
                            <a href="/" id="menu-open-button">menu</a>
                        </span>
                    </div>
                </div>
            </div>
        </nav>

        @yield('content')

        <div class="container">
            <div class="row">
                <br>
                <div class="col-sm-12 text-center">
                    <img src="/images/chmg-final-logo_360.jpg">
                </div>
                <br>
                <div class="col-sm-12 text-center address-home">
                    Coastal Heart Medical Group, Santa Ana<br>2621 S. Bristol St., #108,<br>Santa Ana, CA 92704<br><a href="tel:7147541684">Phone: +1 (714) 754-1684</a>
                </div>
                <br>
                <div class="col-sm-12 text-center address-home">
                    Coastal Heart Medical Group, Garden Grove<br>12665 Garden Grove Blvd., #203,<br>Garden Grove, CA 92843<br><a href="tel:7146382042">Phone: +1 (714) 638-2042</a>
                </div>
                <br>
                <div class="col-sm-12 text-center">
                    <a href="https://www.facebook.com/coastalhmg/"><i class="fa fa-facebook fa-2x"></i></a>
                </div>
                <br>
                <div class="col-sm-12 text-center footer-privacy-text">
                    © Copyright Coastal Heart Medical Group, Inc.
                </div>
                <div class="col-sm-12 text-center footer-privacy-text">
                    Privacy Policy   <br> <a href="https://wbrandstudio.com/" class="wbrand-link" target="_blank">Site development by W Brand Studio</a>
                </div>
                <br>
            </div>
        </div>

        @include('mobile.partials.menu')

        @if(app()->isLocal())
            <script src="{{ asset('js/app.js') }}"></script>
        @else
            <script src="{{ mix('js/app.js') }}"></script>
        @endif
    </body>
</html>
