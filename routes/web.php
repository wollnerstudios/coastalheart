<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/clinical-research', 'PagesController@clinicalResearch');
Route::get('/physician/', 'PagesController@allPhysicians');
Route::get('/physician/{slug}', 'PagesController@physician');

Route::get('/patients/', 'PatientsController@patients');
Route::get('/patients/forms', 'PatientsController@forms');

Route::post('/contact', 'ContactController@submit');

Route::get('/about/', 'AboutController@about');
Route::get('/about/locations', 'AboutController@locations');
Route::get('/thank-you', 'AboutController@thanks');
Route::get('/about/conditions-we-treat', 'AboutController@conditions');
Route::get('/about/staff', 'AboutController@staff');

Route::get('/services/', 'ServicesController@services');
Route::get('/services/angiography-facilities', 'ServicesController@angiographyFacilities');
Route::get('/services/diagnostic-testing', 'ServicesController@diagnosticTesting');
Route::get('/services/venous-studies-treatments', 'ServicesController@venousStudiesTreatments');
Route::get('/services/spider-vein-treatments', 'ServicesController@spiderVeinTreatments');
