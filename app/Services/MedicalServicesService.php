<?php

namespace App\Services;

class MedicalServicesService
{
    protected $data;

    public function __construct()
    {
        $this->data = collect(require 'data/services.php');
    }

    public function all()
    {
        return $this->data;
    }
}
