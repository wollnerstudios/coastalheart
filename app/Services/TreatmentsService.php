<?php
namespace App\Services;


class TreatmentsService
{
    protected $data;

    public function __construct()
    {
        $this->data = collect(require 'data/treatments.php');
    }

    public function all()
    {
        return $this->data;
    }
}