<?php

namespace App\Services;

class ConditionsService
{
    protected $data;

    public function __construct()
    {
        $this->data = collect(require 'data/conditions.php');
    }

    public function all()
    {
        return $this->data;
    }
}
