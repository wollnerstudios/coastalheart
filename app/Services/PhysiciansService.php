<?php

namespace App\Services;

class PhysiciansService
{
    protected $data;

    public function __construct()
    {
        $this->data = collect(require 'data/physicians.php');
    }

    public function all()
    {
        return $this->data;
    }
}
