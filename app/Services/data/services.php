<?php

return [
    [
        'name' => 'heart and vascular services',
        'title' => 'Angiography Facilities',
        'slug' => 'angiography-facilities',
        'image' => 'angiograph.jpg',
        'description' => <<<EOD
<p>
    Coastal Heart Medical Group is the only medical group in Orange County, California with its own in-house Angiographic
    Facilities. MR angiography [MRA] uses a powerful magnetic field, radio waves and a computer to evaluate blood vessels
    and help identify abnormalities or diagnose atheroscelortic [plaque] disease. This exam does not use ionizing radiation
    and may require an injection ofa contrast material called gadolinium, which is less likely to cause an allergice
    reaction than iodinated contrast material.
</p>

<h4 class="text-theme-brown">Our services include:</h4>

<ul>
    <li>Identify narrowed, enlarged, and blocked blood vessels</li>
    <li>Identify blocked and hardened</li>
    <li>Test for blood clots</li>
    <li>Venous ablation</li>
</ul>
EOD
    ],
    [
        'name' => 'Non-Invasive Testing',
        'title' => 'Services: Diagnostic Testing',
        'slug' => 'diagnostic-testing',
        'image' => 'diagnostic.jpg',
        'description' => <<<EOD
<p>
    Our facilities in Orange County include a wide range of noninvasive cardiovasular capabilities. Our experienced
    cardiologists use the latest technology to detect heart disease and discover potential problems. Our available
    testing services include:
</p>

<ul>
    <li>Treadmill stress</li>
    <li>Echocardiogram and Doppler plus color flow imaging</li>
    <li>Nuclear stress test</li>
    <li>Arterial ultrasound and ABI (?) -</li>
    <li>Holder monitoring</li>
    <li>Arrythmia management</li>
</ul>
EOD
    ],
    [
        'name' => 'Venous Studies & Treatments',
        'title' => 'Services: Venous Studies',
        'slug' => 'venous-studies',
        'image' => 'venous-studies.jpg',
        'description' => <<<EOD
<p>
    Venous studies are performed at Coastal Heart’s advanced vascular diagnostic and treatment center, to detect the
    location and severity of venous and arterial vascular diseases. These are state-of-the-art, cost-effective and
    painless procedures, performed by our cardiologists who are credentialed in vascular angiography medicine and
    vascular intervention. We are proud to announce that just opened our own interventional suite for varicose diseases.
</p>

<ul>
    <li><span (click)="expand($event)">ACUTE Venous clots (click to expand)</span></li>
    <li><span (click)="expand($event)">CHRONIC Varicose veins and venous insufficiency (click to expand)</span></li>
    <li><span (click)="expand($event)">CHRONIC Venous Filters and Retrieval (click to expand)</span></li>
    <li><span (click)="expand($event)">CHRONIC Phlebectomy (click to expand)</span></li>
    <li><span (click)="expand($event)">CHRONIC Sclerotherapy (click to expand)</span></li>
    <li><span (click)="expand($event)">CHRONIC Varithena (click to expand)</span></li>
</ul>
EOD
    ]
];
