<?php

return [
    [
        'name' => 'Dr. Anil V. Shah, MD, FACC, FSCAI',
        'slug' => 'shah',
        'avatar' => 'doctors/Dr-shah-md.jpg',
        'mobile_avatar' => 'doctors/dr-shah-mobile.jpg',
        'page_title' => 'Dr. Anil Shah | Cardiologist | Santa Ana',
        'page_description' => 'For over 30 years, Dr. Shah has specialized in treating a wide variety of cardiac and vascular conditions, and is the director of angiography in Santa Ana.',
        'page_keywords' => 'cardiologist, cardiologist santa ana, cardiologist near me  ',
        'bio' => <<<EOD
Dr. Anil Shah is a highly experienced interventional cardiologist specializing in treating a wide variety of cardiac and vascular conditions for over 30 years. He has been in private practice since 1982 after his research fellowship from UCLA and has performed thousands of angioplasties, stent implants and peripheral arterial and peripheral venous interventions and pacemakers.
<br><br>As an experienced researcher, he is interested in cardiac and vascular imaging with the latest non-invasive techniques including CT, nuclear, and ultrasound methods. Dr. Shah has incorporated these non-invasive techniques into his practice, reducing the need for invasive diagnostic testing. Dr. Shah leads as a principal investigator in many clinical trials.
<br><br>Dr. Shah has been awarded several community leadership awards and was awarded ‘Visionary of the Year’ by the community colleges in Orange County. Dr. Shah’s extensive clinical knowledge and experience with patients have led him to become an excellent physician. His ability to bond with patients and make them laugh is something they truly adore.
<br><br>Dr. Shah is heavily focused on interventions for complex peripheral arterial disease and venous management with focus on amputation prevention for diabetics. Dr Shah is the director of the state of the art angiography suite at Coastal Heart.
EOD
    ],
    [
        'name' => 'Amer R. Zarka, MD, FACC',
        'slug' => 'zarka',
        'avatar' => 'doctors/dr-zarka.jpg',
        'mobile_avatar' => 'doctors/dr-zarka-mobile.jpg',
        'page_title' => 'Dr. Amer Zarka | Cardiologist | Garden Grove & Santa Ana',
        'page_description' => 'Dr Zarka received his medical degree, followed by cardiology fellowship & interventional cardiology fellowship at St. Michael’s Medical Center, New Jersey.',
        'page_keywords' => 'cardiologist, cardiologist santa ana, cardiologist garden grove ',
        'bio' => <<<EOD
Dr. Amer Zarka is an experienced interventional cardiologist. He received his medical degree from the University of Jordan, followed by residency, cardiology fellowship and interventional cardiology fellowship at St. Michael’s Medical Center in Newark, NJ.
<br><br>Prior to joining CHMG, Dr. Zarka spent a few years as an interventional cardiologist at Southern California Permanente Medical Group, a decade at UC Irvine as an Asst. Clinical Professor of Medicine, and three years as the Director of Cardiology at the Hyperlipidemia Clinic. He also holds nine prestigious certifications and is a fellow of the American College of Cardiology.
<br><br>When Dr. Zarka is not practicing medicine, he is writing or contributing to books and articles, and conducting research projects. In fact, he was awarded a grant from the Veteran Administration RAG for the study of electrocardiographic body surface potential mapping.
EOD
    ],
    [
        'name' => 'Mohammad A. Hajjar, MD',
        'slug' => 'hajjar',
        'avatar' => 'doctors/Mohammad-A-Hajjar.jpg',
        'mobile_avatar' => 'doctors/dr-hajjar-mobile.jpg',
        'page_title' => 'Dr. Mohammad A. Hajjar | Cardiologist | Garden Grove & Santa Ana',
        'page_description' => 'Dr. Hajjar is an experienced interventional cardiologist who performs all ranges of coronary artery interventions in Garden Grove and Santa Ana, CA. ',
        'page_keywords' => 'cardiologist, cardiologist santa ana, cardiologist garden grove ',
        'bio' => <<<EOD
Dr. Mohammad A. Hajjar is an experienced interventional cardiologist. He received his medical degree from University of Aleppo, Syria, flowed by residency at the University of Tennessee, cardiology fellowship at Michigan State University and interventional cardiology fellowship at the University of Texas-Galveston.

<br><br>Dr. Hajjar is board certified in internal medicine, cardiology, interventional cardiology, echocardiography and nuclear cardiology. He performs all ranges of coronary artery interventions from balloon angioplasty, stent placement and atherectomy to chronic total occlusion interventions.  Dr. Hajjar also performs peripheral artery interventions.

<br><br>Dr. Hajjar has many awards and publications, including hospitalist of the month and graduating top of his class in 2000.
EOD
    ],
    [
        'name' => 'Salam A. Sbaity, MD',
        'slug' => 'sbaity',
        'avatar' => 'doctors/salam-sbaity.jpg',
        'mobile_avatar' => 'doctors/dr-sbaity-mobile.jpg',
        'page_title' => 'Dr. Salam Sbaity | Cardiologist | Santa Ana',
        'page_description' => 'Dr Sbaity is an experienced caridac electrophysiologist who performs wide range of cardiac device placement and ablation procedures in Santa Ana.',
        'page_keywords' => 'cardiologist, cardiologist santa ana, cardiologist near me',
        'bio' => <<<EOD
Dr. Salam Sbaity is an experienced cardiac electrophysiologist. He received his medical degree from the prestigious American University of Beirut, Lebanon, followed by residency, cardiology fellowship and cardiac electrophysiology fellowship at the University of Iowa.

<br><br>Dr. Sbaity is board certified in internal medicine, cardiology and cardiac electrophysiology. He is a fellow of the American College of Cardiology and member of the heart rhythm society. He performs all ranges of cardiac electrophysiology procedure, including device placement and ablation.

<br><br>Dr. Sbaity has many academic honors, including being Ranked #1 in Lebanon in the Lebanese Baccalaureate, the Award of the Lebanese Ministry of Education, and the “INMAA” Organization Award.
EOD
    ],
//    [
//        'name' => 'Kamil Muhyieddeen, MD',
//        'slug' => 'muhyieddeen',
//        'avatar' => 'doctors/Kamil.jpg',
//        'mobile_avatar' => 'doctors/dr-muhyieddeen-mobile.jpg',
//        'page_title' => 'Dr. Kamil Muhyieddeen | Interventional Cardiologist',
//        'page_description' => 'Dr. Muhyiedden is an experienced interventional cardiologist and has over 10 publications in the field of cardiology, he was also awarded intern and fellow of the year.',
//        'page_keywords' => 'Kamil Muhyieddeen, cardiologist, medical degree, echocardiography, cardiology',
//        'bio' => <<<EOD
//Dr. Kamil Muhyieddeen is an experienced interventional cardiologist. He received his medical degree from the University of Jordan, followed by residency at the prestigious Houston Methodist hospital, cardiology fellowship at the UCSF-Fresno and interventional cardiology fellowship at the University of Wisconsin-Madison.
//
//<br><br>Dr. Muhyiedden is board certified in internal medicine, cardiology and echocardiography. He performs all ranges of coronary and peripheral artery interventions. In addition he is highly trained in performing structural heart disease, including Trans-Aortic Valve Implantation (TAVI) and Atrial Septal Defect (ASD)/Patent Foramen Ovale (PFO) closure.
//
//<br><br>Dr. Muhyiedden has over 10 publications in the field of cardiology. He was also awarded intern and fellow of the year.
//EOD
//    ],
    [
        'name' => 'Mercedeh Esmaeili, NP, MSN, RN',
        'slug' => 'esmaeili',
        'avatar' => 'doctors/mercedeh-esmaeili-md.jpg',
        'mobile_avatar' => 'doctors/dr-esmaeili-mobile.jpg',
        'page_title' => 'Mercedeh Esmaeili  | Nurse Practitioner',
        'page_description' => 'Mercedeh is a well-educated and highly trained member of Coastal Heart Medical Group, and is devoted to always keeps the focus on patient satisfaction.',
        'page_keywords' => 'cardiologist near me, cardiologist, medical group',
        'bio' => <<<EOD
Mercedeh Esmaeili is a young, yet highly-experienced and versatile medical professional, who has served as a nurse practitioner, cardiovascular critical care registered nurse, and a surgical registered nurse. She has two years of clinical NP experience and five years of RN experience working in critical care and high patient volume settings for both inpatient and outpatient.

<br><br>Receiving a BS in Nursing from California State University Fullerton and a Family Nurse Practitioner Masters degree from Azusa Pacific University, Mercedeh is a well-educated and highly trained member of Coastal Heart Medical Group. Her outstanding academic achievements also include being a member of the Sigma Theta Tau International Honor Society of Nursing while studying for her Masters degree at PAU.

<br><br>Mercedeh is a truly devoted caregiver who always keeps the focus on patient satisfaction. With her exceptional academic background and number of certifications, Mercedeh is undoubtedly well equipped to serve patients in many levels of care. Her dedication to go above and beyond her duties helps contribute to the stellar reputation of Costal Heart Medical Group.
EOD
    ],
    [
        'name' => 'Tracy Arsenault, PA-C',
        'slug' => 'arsenault',
        'avatar' => 'doctors/tracy.jpg',
        'mobile_avatar' => 'doctors/ms-arsenault-mobile.jpg',
        'page_title' => 'Tracy Arsenault | Certified Physicians Assistant',
        'page_description' => 'Tracy has completed several core & elective rotations at many fine institutions. Tracy attended Springfield College & received a MS in Exercise Physiology. ',
        'page_keywords' => 'cardiologist near me, cardiologist, medical group',
        'bio' => <<<EOD
Tracy Arsenault is an excellent Certified Physicians Assistant who joins the Coastal Heart Medical Group team from a long history of residence stints all over the country. Tracy received her education and training from many fine institutions including Northwestern Medical Faculty Foundation and Advocate Christ Medical Center. She received her MS from Cornell University and the Weill Cornell Graduate School of Medical Sciences. Tracy also attended Springfield College where she received her MS in Exercise Physiology.

<br><br>Throughout her career Tracy completed many core and elective rotations at notable institutions such as New York Presbyterian Hospital, Columbian University Medical Center, Thoracic Surgery New York Presbyterian Hospital, and Cardiovascular Intensive Care Unit New York Hospital.
EOD
    ],
];
