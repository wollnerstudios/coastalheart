<?php

return [
    [
        'name' => 'Arrhythmia',
        'description' => 'Cardiac arrhythmia, also known as cardiac dysrhythmia or irregular heartbeat, is a group of conditions in which the heartbeat is irregular, too fast, or too slow. A heart rate that is too fast – above 100 beats per minute in adults – is called tachycardia and a heart rate that is too slow – below 60 beats per minute – is called bradycardia. Many types of arrhythmia have no symptoms. When symptoms are present these may include palpitations or feeling a pause between heartbeats. More seriously there may be lightheadedness, passing out, shortness of breath, or chest pain. While most types of arrhythmia are not serious, some predispose a person to complications such as stroke or heart failure. Others may result in cardiac arrest.'
    ],
    [
        'name' => 'Atrial Fibrillation',
        'description' => 'Atrial fibrillation (AF or A-fib) is an abnormal heart rhythm characterized by rapid and irregular beating. Often it starts as brief periods of abnormal beating which become longer and possibly constant over time. Most episodes have no symptoms. Occasionally there may be heart palpitations, fainting, lightheadedness, shortness of breath, or chest pain. The disease is associated with an increased risk of heart failure, dementia, and stroke. It is a type of supraventricular tachycardia.'
    ],
    [
        'name' => 'Atrial Flutter',
        'description' => 'Atrial flutter (AFL) is a common abnormal heart rhythm that starts in the atrial chambers of the heart. When it first occurs, it is usually associated with a fast heart rate (100 or more heartbeats per minute), and is classified as a type of supra-ventricular tachycardia. Although this abnormal heart rhythm typically occurs in individuals with cardiovascular disease (e.g. high blood pressure, coronary artery disease, and cardiomyopathy) and diabetes mellitus, it may occur spontaneously in people with otherwise normal hearts. It is typically not a stable rhythm, and often degenerates into atrial fibrillation (AF). However, it does rarely persist for months to years.'
    ],
//    [
//        'name' => 'Cardiac Tumors',
//        'description' => 'The most common primary tumor of the heart is the myxoma. In surgical series, the myxoma makes up as much as 77% of all primary tumors of the heart. Less common tumors of the heart include lipoma and cystic tumor of the atrioventricular nodal region. About 20 percent of primary tumors of the heart are malignant in nature. Malignant tumors of the heart include rhabdomyosarcomas, angiosarcomas, myxosarcomas, fibrosarcomas, leiomyosarcomas, reticulum cell sarcomas, desmoplastic small round cell tumor,and liposarcomas. The cardiac sarcomas may occur at any age, but are more commonly seen in individuals in their 20s to 40s. They occur equally in males and females.'
//    ],
    [
        'name' => 'Cardiomyopathy',
        'description' => 'Cardiomyopathy is a group of diseases that affect the heart muscle. Early on there may be few or no symptoms. Some people may have shortness of breath, feel tired, or have swelling of the legs due to heart failure. An irregular heart beat may occur as well as fainting. Those affected are at an increased risk of sudden cardiac death. Types of cardiomyopathy include hypertrophic cardiomyopathy, dilated cardiomyopathy and restrictive cardiomyopathy. In hypertrophic cardiomyopathy the heart muscle thickens. In dilated cardiomyopathy the ventricles enlarge and weaken. In restrictive cardiomyopathy the ventricle stiffens.'
    ],
    [
        'name' => 'Coronary Artery Disease',
        'description' => 'Coronary artery disease (CAD), also known as ischemic heart disease (IHD), is a group of diseases that includes: stable angina, unstable angina, myocardial infarction, and sudden cardiac death. It is within the group of cardiovascular diseases of which it is the most common type. A common symptom is chest pain or discomfort which may travel into the shoulder, arm, back, neck, or jaw. Occasionally it may feel like heartburn. Usually symptoms occur with exercise or emotional stress, last less than a few minutes, and get better with rest. Shortness of breath may also occur and sometimes no symptoms are present. Other complications include heart failure or an irregular heartbeat.'
    ],
    [
        'name' => 'Diabetes Related Complications',
        'description' => 'Diabetes is a disease affecting millions. In diabetic patients, the pancreas does not work properly affecting a person’s blood sugar levels. Wider health problems accelerate the deleterious effects of diabetes. When blood sugar levels are not controlled, diabetics have a higher risk of hypertension, obesity, anxiety, fatigue, heart attack, stroke, peripheral arterial disease, kidney failure and blindness. The complications of diabetes mellitus are far less common and less severe in people who have well-controlled blood sugar levels.'
    ],
    [
        'name' => 'Heart Failure',
        'description' => 'Heart failure (HF), often referred to as congestive heart failure (CHF), occurs when the heart is unable to pump sufficiently to maintain blood flow to meet the body\'s needs. Signs and symptoms commonly include shortness of breath, excessive tiredness, and leg swelling. The shortness of breath is usually worse with exercise, while lying down, and may wake the person at night. A limited ability to exercise is also a common feature. Chest pain, including angina, does not typically occur due to heart failure. Common causes of heart failure include coronary artery disease including a previous myocardial infarction (heart attack), high blood pressure, atrial fibrillation, valvular heart disease, excess alcohol use and infection.'
    ],
    [
        'name' => 'Hypertension',
        'description' => 'Hypertension (HTN or HT), also known as high blood pressure (HBP), is a long term medical condition in which the blood pressure in the arteries is persistently elevated. High blood pressure usually does not cause symptoms. Long term high blood pressure, however, is a major risk factor for coronary artery disease, stroke, heart failure, peripheral vascular disease, vision loss, and chronic kidney disease. High blood pressure is classified as either primary (essential) high blood pressure or secondary high blood pressure. About 95% of cases are primary, defined as high blood pressure due to nonspecific lifestyle and genetic factors. Lifestyle factors that increase the risk include excess salt, excess body weight, smoking, and alcohol. The remaining 5% of cases are categorized as secondary high blood pressure, defined as high blood pressure due to an identifiable cause, such as chronic kidney disease, narrowing of the kidney arteries, an endocrine disorder, or the use of birth control pills.'
    ],
//    [
//        'name' => 'Hyperlipidemia/Hyperlipidemias',
//        'description' => 'Hypertrophic cardiomyopathy (HCM) is a disease in which the myocardium (heart muscle) is hypertrophic (enlarged) without any obvious cause, creating functional impairment of the heart. It is the leading cause of sudden cardiac death in young athletes. The occurrence of hypertrophic cardiomyopathy is a significant cause of sudden cardiac death in any age group and a cause of disabling cardiac symptoms. HCM is frequently asymptomatic until sudden cardiac death, and for this reason some suggest routinely screening certain populations for this disease. In most patients, HCM is associated with little or no disability and normal life expectancy. Diagnosis is usually by echocardiogram. While there is no known prevention or cure, symptomatic patients may be treated effectively by medication or, in severe cases, by surgery. A cardiomyopathy is a disease that affects the muscle of the heart. With HCM, the myocytes (cardiac contractile cells) in the heart increase in size, which results in the thickening of the heart muscle. In addition, the normal alignment of muscle cells is disrupted, a phenomenon which is known as myocardial disarray.'
//    ],
    [
        'name' => 'Hyperlipidemia/Hyperlipidemias',
        'description' => 'Hyperlipidemias are divided into primary and secondary subtypes. Primary hyperlipidemia is usually due to genetic causes (such as a mutation in a receptor protein), while secondary hyperlipidemia arises due to other underlying causes such as diabetes. Lipid and lipoprotein abnormalities are common in the general population and are regarded as modifiable risk factors for cardiovascular disease due to their influence on atherosclerosis. In addition, some forms may predispose to acute pancreatitis.'
    ],
    [
        'name' => 'Hypertrophic Cardiomyopathy',
        'description' => "Hypertrophic cardiomyopathy (HCM) is a disease in which the myocardium (heart muscle) is hypertrophic (enlarged) without any obvious cause, creating functional impairment of the heart. It is the leading cause of sudden cardiac death in young athletes. The occurrence of hypertrophic cardiomyopathy is a significant cause of sudden cardiac death in any age group and a cause of disabling cardiac symptoms. HCM is frequently asymptomatic until sudden cardiac death, and for this reason some suggest routinely screening certain populations for this disease. In most patients, HCM is associated with little or no disability and normal life expectancy. Diagnosis is usually by echocardiogram. While there is no known prevention or cure, symptomatic patients may be treated effectively by medication or, in severe cases, by surgery. A cardiomyopathy is a disease that affects the muscle of the heart. With HCM, the myocytes (cardiac contractile cells) in the heart increase in size, which results in the thickening of the heart muscle. In addition, the normal alignment of muscle cells is disrupted, a phenomenon which is known as myocardial disarray."
    ],
    [
        'name' => 'Peripheral Arterial Disease (PAD)',
        'description' => 'A progressive condition that, when left untreated, can cause severe leg pain and therefore reducing mobility. If left untreated, there is a risk of amputation of the foot or lower leg.'
    ],

    [
        'name' => 'Chronic Venous Insufficiency (CVI)',
        'description' => 'A condition caused by faulty valves that allow blood to pool in the leg veins, which can result in painful symptoms.',
    ],

    [
        'name' => 'Diabetic Foot Ulcers & Foot Pain',
        'description' => 'Diabetes affects small and large arteries. Arterial disease restricts blood flow to the legs and feet, which will eventually cause ulcers and gangrene. An estimated 24% of patients with diabetes who develop a foot ulcer will require amputation.',
    ],

    [
        'name' => 'Varicose Veins',
        'description' => 'Varicose veins is an initial finding for venous hypertension. It then progresses to leg pain, swelling, discoloration of the legs, itching, leg fatigue and ultimately venous ulcers. Coastal Heart Medical Group prides itself in treating all forms of venous disease from the simple to the most complex, including acute venous diseases. Spider veins are also treated at this facility.',
    ],

    [
        'name' => 'Critical Limb Ischemia',
        'description' => 'A condition caused by an obstruction in the arteries which reduces blood flow and causes severe pain and ulcers. This condition needs to be urgently treated or it may progress into gangrene and potential loss of limb through amputation. Coastal Heart Medical Group prides itself in being one of the few practices treating this condition through its own state-of-the-art angiographic suite with catheter device techniques.',
    ],

    [
        'name' => 'Leg and Foot Swelling',
        'description' => 'Known as “edema,” leg and/or foot swelling can be cause by numerous factors, including venous insufficiency, congestive heart failure, kidney and liver disease, protein malnutrition, DVT, and a host of other conditions.',
    ]
];
