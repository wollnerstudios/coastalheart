<?php

return [
//    [
//        'name' => 'ACUTE Venous clots',
//        'description' => <<<EOD
//<p>
//    Acute thrombus needs to be treated in less than 15 days. A common type of venous thrombosis (local
//    coagulation or clotting of the blood), is a deep vein clotting of the blood (thrombosis – “DVT”), in
//    the deep veins of the leg. If the thrombus breaks off (embolizes) and flows towards the lungs, it can
//    become a pulmonary embolism (PE), a blood clot in the lungs. The goal in the treatment of acute DVT is
//    to restore blood flow. Once the clot is removed or dissolved, swelling and pain typically resolve.
//    Treatments available at Coastal Heart are:
//</p>
//<ul>
//    <li>medications to dissolve the clot</li>
//    <li>mechanical thrombectomy (breaks down the clot into smaller pieces that can be sucked out of the blood vessel)</li>
//</ul>
//EOD
//    ],
    [
        'name' => 'Varicose veins and venous insufficiency',
        'description' => <<<EOD
<p>
    Varicose veins, or veins on the leg as they are commonly referred to, are veins that have become
    enlarged and twisted. As you get older, your veins can lose elasticity, causing them to stretch. They
    can be more than just a cosmetic issue and in some cases can cause swelling, discomfort, pain,
    fatigue, tiredom and itchiness. In advanced cases, one may develop ulcers. If you suffer from varicose
    veins, there are a variety of treatment options available to you at Coastal Heart Medical Group. We
    are a leading vein removal clinic in Orange County specialized in all kinds of vein removal procedures.
</p>
EOD
    ],
//    [
//        'name' => 'Venous Filters and Retrieval',
//        'description' => <<<EOD
//<p>
//    IVC filters are essential for treating patients with deep venous thrombosis who have contraindications
//    to anticoagulation. However, they do not come without risk of complications. Therefore, it is
//    important to choose a professional and experienced medical facility for the procedures. Orange Coast
//    Heart Medical Group has extensive experience in….
//</p>
//EOD
//    ],
//    [
//        'name' => 'Phlebectomy',
//        'description' => <<<EOD
//<p>
//    Plebectomy is a technique to remove varicose veins. In this procedure, several tiny cuts (incisions)
//    are made in the skin through which the varicosed vein is removed. It’s highly successful when
//    performed in patients who are good candidates. This is usually done in the office of one of our
//    physicians using local anesthesia.
//</p>
//EOD
//    ],
    [
        'name' => 'Sclerotherapy',
        'description' => <<<EOD
<p>
    Sclerotherapy is a medical procedure used to eliminate varicose veins and spider veins. Sclerotherapy involves an injection of a solution directly into the vein. The solution causes the vein to scar, forcing blood to reroute through healthier veins.
</p>
EOD
    ],
    [
        'name' => 'Varithena',
        'description' => <<<EOD
<p>
    Varithena® (polidocanol injectable foam) is the first and only FDA–approved foam for the treatment of
    incompetent Great Saphenous Veins (GSV) and visible varicosities of the GSV system above and below the
    knee. The GSV is the longest vein in the body running along the length of the lower limb. Treatment
    with Varithena® is a minimally invasive, nonsurgical procedure for the treatment of varicose veins
    that only requires an ultrasound machine and standard medical supplies.
</p>
EOD
    ]
];
