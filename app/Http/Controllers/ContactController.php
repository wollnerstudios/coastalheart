<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormSubmission;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view($this->getView('contact'));
    }

    public function submit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'location' => 'required',
            'body' => 'required',
        ]);

        \Mail::to(env('CONTACT_FORM_TARGET', 'nick@wbrandstudio.com'))->send(new ContactFormSubmission($request->all()));

        return view('desktop.thank-you');
    }
}
