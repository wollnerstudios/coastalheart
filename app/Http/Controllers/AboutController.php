<?php

namespace App\Http\Controllers;

use App\Services\ConditionsService;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function about()
    {
        $conditions = (new ConditionsService())->all()->toArray();
        $meta = [
            'title' => '',
            'description' => '',
            'keywords' => '',
        ];
        
        return view($this->getView('about.index'),  compact('conditions'));
    }

    public function locations()
    {
        $meta = [
            'title' => 'Our Locations | Coastal Heart Medical Group',
            'description' => 'We have state of the art facilities located in both Santa Ana and Garden Grove to serve serve patients throughout Southern California.',
            'keywords' => 'orange county medical group, medical orange county ',
        ];

        return view($this->getView('about.locations'), compact('meta'));
    }

    public function thanks()
    {
        $meta = [
            'title' => 'Our Locations | Coastal Heart Medical Group',
            'description' => 'We have state of the art facilities located in both Santa Ana and Garden Grove to serve serve patients throughout Southern California.',
            'keywords' => 'orange county, medical facility, santa ana, garden grove, california, coastal heart medical group, medical group, cardiac conditions, cardiac solutions, vascular conditions, vascular conditions, health care medical group, healthcare medical group, healthcare urgent care, cardiovascular specialists, heart conditions, heart solutions, heart disease',
        ];

        return view('desktop.thank-you');
    }

    public function conditions()
    {
        $conditions = (new ConditionsService())->all();
        $meta = [
            'title' => 'About Us | Conditions We Treat',
            'description' => 'See our some of the most common and complex heart and vascular diseases our experienced team at Coastal Heart Medical Group treats.',
            'keywords' => 'medical group, sclerotherapy, varicose veins treatment, cardiologist near me, cardiologist',
        ];

        return view($this->getView('about.conditions'), compact('conditions', 'meta'));
    }

    public function staff()
    {
        $meta = [
            'title' => 'Our Staff | Coastal Heart Medical Group',
            'description' => 'At Coastal Heart Medical Group, our wonderful and dedicated staff have provided the highest level of care utilizing the latest technology for over 30 years.',
            'keywords' => 'orange county medical group, medical orange county, cardiologist near me ',
        ];

        return view($this->getView('about.staff'), compact('meta'));
    }
}
