<?php

namespace App\Http\Controllers;

use App\Services\PhysiciansService;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        $physicians = (new PhysiciansService())->all();

        return view($this->getView('home'), compact('physicians'));
    }

    public function clinicalResearch()
    {
        $meta = [
            'title' => 'Clinical Research | Coastal Heart Medical Group',
            'description' => 'Clinical research and studies are vital, and allow us to provide innovative treatments to our patients who are involved in these investigative medical trials.',
            'keywords' => 'medical research, clinical trials, medical study, clinic, coastal heart, treatment',
        ];

        return view($this->getView('clinical-research'), compact('meta'));
    }

    public function physician($slug)
    {
        $physicians = (new PhysiciansService())->all();
        $physician = $physicians->where('slug', $slug)->first();

        $index = null;

        foreach ($physicians->values() as $i => $physician) {
            if ($slug == $physician['slug']) {
                $index = $i;
                break;
            }
        }

        $prevPhysician = $physicians->values()[$index - 1] ?? $physicians->values()[$physicians->count() - 1];
        $nextPhysician = $physicians->values()[$index + 1] ?? $physicians->values()[0];

        $meta = [
            'title' => $physician['page_title'],
            'description' => $physician['page_description'],
            'keywords' => $physician['page_keywords'],
        ];

        return view($this->getView('physician'), compact('physicians', 'physician', 'nextPhysician', 'prevPhysician', 'meta'));
    }

    public function allPhysicians()
    {
        $physicians = (new PhysiciansService())->all()->toArray();

        foreach ($physicians as $key => $value) {
            # code...
            $physicians[$key]['shortBio'] = strstr($physicians[$key]['bio'], "<br>", true);
        }

        return view($this->getView('physicians'), compact('physicians'));
    }
}
