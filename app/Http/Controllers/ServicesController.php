<?php

namespace App\Http\Controllers;

use App\Services\TreatmentsService;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function services()
    {
        $treatments = (new TreatmentsService())->all();
        $meta = [
            'title' => '',
            'description' => '',
            'keywords' => '',
        ];

        return view($this->getView('services.index'),compact('treatments'));
    }

    public function angiographyFacilities()
    {
        $meta = [
            'title' => 'Angiography Facility | Orange County CA',
            'description' => 'Coastal Heart Medical Group is the only office in Orange County, CA with its own Angiography Facilities to perform, diagnose arterial, & venous narrowing.',
            'keywords' => 'varicose veins treatment, medical group, blood clot treatment',
        ];

        return view($this->getView('services.angiography-facilities'), compact('meta'));
    }

    public function diagnosticTesting()
    {
        $meta = [
            'title' => 'Diagnostic Testing Services | Orange County CA',
            'description' => 'Our diagnostic facility has been designed with state-of-the-art equipment & instruments, including a wide range of non-invasive cardiovascular capabilities. ',
            'keywords' => 'Electrocardiograms, medical clinic near me, medical orange county',
        ];

        return view($this->getView('services.diagnostic-testing'), compact('meta'));
    }

    public function venousStudiesTreatments()
    {
        $treatments = (new TreatmentsService())->all();
        $meta = [
            'title' => 'Venous Studies And Treatments | Coastal Heart Medical Group',
            'description' => 'We\'re able to detect the location and severity of venous and arterial vascular diseases quicker with our in-house diagnostic & treatment center.',
            'keywords' => 'varicose veins treatment, sclerotherapy ',
        ];

        return view($this->getView('services.venous-studies-treatments'), compact('treatments', 'meta'));
    }

    public function spiderVeinTreatments()
    {
        $meta = [
            'title' => 'Spider Vein Treatment | Coastal Heart Medical Group',
            'description' => 'If you suffer from varicose veins, there are a variety of treatment options available to you at Coastal Heart Medical Group.',
            'keywords' => 'spider vein treatment, sclerotherapy ',
        ];

        return view($this->getView('services.spider-vein-treatments'), compact( 'meta'));
    }
}
