<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PatientsController extends Controller
{
    public function patients()
    {
        return view($this->getView('patients.index'));
    }

    public function forms()
    {
        $meta = [
            'title' => 'New Patient Forms | Coastal Heart Medical Group',
            'description' => 'Your time is valuable, which is why we provide new patient forms online so that you can complete these prior to your appointment.',
            'keywords' => 'coastal heart medical group, patient forms ',
        ];

        return view($this->getView('patients.forms'), compact('meta'));
    }
}
